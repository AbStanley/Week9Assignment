import { item } from "./item.interface";

export interface CartCreateResponse {
  data: {
      id: string;
      user_id: string;
      number: string;
      status: string;
      total: string;
      total_items: number;
      completed_at: string;
      created_at: string;
      items: item[]
  }
}