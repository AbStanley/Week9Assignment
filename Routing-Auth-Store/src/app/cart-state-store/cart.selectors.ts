import { createFeatureSelector, createSelector } from '@ngrx/store';
import { InfoProductResponse } from '../interface/produtResponse/infoProductResponse.interface';
import { CartList } from '../interface/store/cartList.interface';
import { Product } from '../interface/store/product.interface';


export interface ProductGroup {
    product: InfoProductResponse;
    count: number; 
  }

export const cartProducts = createSelector(
    createFeatureSelector('cartEntries'),
    (state: CartList) => state
)

export const listOfProductsCart = createSelector(
    createFeatureSelector('cartEntries'),
    (state: InfoProductResponse[]) => {
        return state;
    }
)

export const selectCountProducts = createSelector(
    createFeatureSelector('cartEntries'),
    (state: InfoProductResponse[]) => {
        return state.length;
    }
);

export const selectTotalPrice = createSelector(
    createFeatureSelector('cartEntries'),
    (state: InfoProductResponse[]) => {
      let totalPrice: number = 0;
        state.map(product => {
            totalPrice += Number(product.master.price);
        });
        return totalPrice;
    });
    export const selectGroupedCartEntries = createSelector(
        createFeatureSelector('cartEntries'),
        (state:InfoProductResponse[]) => {
            
          var map: Map<number, ProductGroup> = new Map;
      
        if (state){
        state.map(
            p => {
                console.log(p);
                if (map.get(Number(p.id))) {
                    (map.get(Number(p.id)) as ProductGroup).count++;
                } else {
                    map.set(Number(p.id), { product: p, count: 1 });
                }
            }
        )
    }
          const sortedMap = new Map([...map.entries()].sort());
          return Array.from(sortedMap.values());
        }
      )
    