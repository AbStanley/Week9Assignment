export interface UserResponse {
    "data" : {
        token: string;
        user:{
            id: number;
            name: string;
            email: string;
        }
    }
}