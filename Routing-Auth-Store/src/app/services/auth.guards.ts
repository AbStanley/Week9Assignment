import { Injectable } from "@angular/core";
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { AuthState } from "../auth/auth.reducer";
import { AppState } from "../reducers";

import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router, private store: Store<AuthState>) {}
    
    canActivate(): boolean {
        if (!this.authService.isAuthenticated()) {
            this.router.navigate(["/login"]);
            return false;
        }
        return true;        
    }
/*
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Boolean> {
        return this.store.pipe(
            tap((authState: AuthState) => {
                if (!authState.isAuthenticated) {
                    this.router.navigate(["/login"]);
                }
            })
        );
            
    }*/
}