import { Component, OnInit, Output } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from './reducers';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
import { map } from 'rxjs/operators';
import { pipe, Observable, BehaviorSubject } from 'rxjs';
import { userInfo, isLoggedIn } from './auth/auth.selectors';
import { Logout } from './auth/auth.actions';
import { userHomeInfo } from './interface/user/userHomeInfo.interface';
import { Target } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  profileInfoShow: boolean = false;
  userInfo!: userHomeInfo;
  searchValue: string = '';

  isLoggedIn$?: Observable<boolean>;
  userInfo$?: Observable<any>;

  displayClick: boolean = false;
  

  myValue!: boolean;

  constructor(private authService: AuthService, public dataManager: DataService, private store: Store<any>) {}
  
  ngOnInit() {
    
    this.isLoggedIn$ = this.store.pipe(select(isLoggedIn));
    this.userInfo$ = this.store.pipe(select(userInfo));

    this.store.pipe(select(userInfo)).subscribe((res: userHomeInfo) => {
      //console.log(res);
    });

   
  }



  get AuthService(): AuthService {
    return this.authService;
  }

  showProfileInfo($event: any): void{
    
    this.profileInfoShow = $event;

  }


  getProfileInfo(): void{      
    this.profileInfoShow = true;
  }

  timer?: number;
  waitTime?: number = 1000;

  search($event: any): void{
      $event.preventDefault();
      if($event.target.value != ''){
        clearTimeout(this.timer);
        this.timer = setTimeout(() => {
          this.searchValue = $event.target.value;
          this.dataManager.sendData(this.searchValue);
        }, this.waitTime);
      }      
    
  }

  logoutUser(): void {
    this.store.dispatch(new Logout());
    this.authService.logoutUser();

  }
  
  displayMenu(){

    this.displayClick = !this.displayClick;
  }
}
