import { createFeatureSelector, createSelector } from "@ngrx/store";
import { User } from "../classes/users";
import { UserResponse } from "../interface/user/user.interface";

export const selectAuthState = (state: any) => state;

export const isLoggedIn = createSelector(
  createFeatureSelector("auth"),
  (auth: any) => auth.isAuthenticated 
);

export const userInfo = createSelector(
  createFeatureSelector("auth"),
  (auth:any) => auth
);
