export interface Add_UpdateProduct {
    "data": {
        "items": [
            {
                "product_variant_id": number,
                "quantity": number
            }
        ]
    }
}

export interface QuantityUpdate {
    data: {
        items: [
            {
                id: number,
                quantity: number
            }
        ]
    }
}