import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HomeProfileComponent } from './components/home-profile/home-profile.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AuthService } from './services/auth.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './services/auth.guards';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { LoggedInGuard } from './services/logged-in.guard';
import { DataService } from './services/data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { reducers, metaReducers } from './reducers';
import * as fromAuth from './auth/auth.reducer';
import * as fromCart from './cart-state-store/cart.reducer';
import { cartReducer, metaReducerLocalStorage } from './cart-state-store/cart.reducer';
import { ShopProductsComponent } from './components/store/shop-products/shop-products.component';
import { ShopCartComponent } from './components/store/shop-cart/shop-cart.component';
import { HeaderComponent } from './components/store/header/header.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeProfileComponent,
    NotFoundComponent,
    UserInfoComponent,
    ShopProductsComponent,
    ShopCartComponent,
    HeaderComponent    
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    
    RouterModule.forRoot([
      { 
        path: '', 
        component: LoginComponent,
        canActivate: [LoggedInGuard]
      },
      {
        path: 'register',
        component: RegisterComponent,
        //canActivate: [LoggedInGuard]
      },
      {
        path: 'login',
        component: LoginComponent,
       // canActivate: [LoggedInGuard]

      },
      {
        path: 'home',
        component: HomeProfileComponent,
        //canActivate: [AuthGuard]
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ]),
    
    BrowserAnimationsModule,
    MatSelectModule,
    MatOptionModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    StoreModule.forRoot({cartEntries: cartReducer}, { metaReducers: [ metaReducerLocalStorage]}),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.authReducer),
    


  ],
  providers: [
    AuthService,
    AuthGuard,
    LoggedInGuard,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
