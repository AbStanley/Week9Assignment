import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Register } from '../classes/register';
import { User } from '../classes/users';
import { loginResponse } from '../classes/loginResponse';
import { JwtHelperService } from '@auth0/angular-jwt';

import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { UserResponse } from '../interface/user/user.interface';
import { userHomeInfo } from '../interface/user/userHomeInfo.interface';
import { InfoProductResponse } from '../interface/produtResponse/infoProductResponse.interface';
import { CartCreateResponse } from '../interface/store/cartCreateResponse.interface';
import { Add_UpdateProduct, QuantityUpdate } from '../interface/store/App_UpdateProduct.interface';
import { RemoveProduct } from '../interface/store/removeProduct.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  
  constructor(private http: HttpClient) {}

  token!: string;
  helper = new JwtHelperService();
  activeUser?: userHomeInfo;
  addedItems?: any;

  getProfileInfo():  userHomeInfo | undefined {
  
      return this.activeUser;
  }
  
  /*Observable<userHomeInfo> {
    
    
    return this.http
      .post<userHomeInfo>(
        'https://api/v1/users/login',
        {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        }
      )
      .pipe(
        map((response) => {
          return response;
        }),
        catchError((error) => {
          return throwError(error);
        })
      );
  }*/

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    throw new Error('Method not implemented.');
  }

  loginUser(credentials: User): Observable<UserResponse>{
    return this.http
      .post<UserResponse>(
        'https://trainee-program-api.applaudostudios.com/api/v1/users/login',
        credentials
      ).pipe
      (
        map((response) => {
          
          this.token = response.data.token;
          localStorage.setItem('token', this.token);
          this.activeUser = response.data.user;
          return response;
        }),
        catchError((error) => {
          return throwError(error);
        })
      );
  }

  logoutUser(): void {
    localStorage.removeItem('token');
    this.token = '';
    this.isLoggedIn();
  }

  registerUser(user: Register): Observable<Object> {
    return this.http.post(
      'https://sheltered-oasis-97086.herokuapp.com/auth/signup',
      user
    );
  }

  isLoggedIn() {
    return this.helper.isTokenExpired(this.token);
  }

  logout() {
    localStorage.removeItem('token');
   localStorage.removeItem('state'); 
  }

  isAuthenticated(): boolean {
    const accessToken = localStorage.getItem('token');
    return !!(accessToken && this.helper.isTokenExpired(accessToken) === false);
  }

  getListOfProducts(): any {
    return this.http.get('https://trainee-program-api.applaudostudios.com/api/v1/products?include=image_attachment.blob,master,category').
    pipe(
      map((response: any) => {
        return response;
      }),
      catchError((error) => {
        console.log('Next error is just a sign of an empty array:');
        return throwError(error);
      })
    );
  }

  likesManager($event: any, action: string){
    let data = {
      "data": {
        "product_id": $event.id,
        "kind": action
      }
    };

    let header = {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + localStorage.getItem('token')),  
    }

    return this.http.post('https://trainee-program-api.applaudostudios.com/api/v1/likes', data, header).
    pipe(
      map((response: any) => {
        return response;
      }
      ),
      catchError((error) => {
        return throwError(error);
      }
      )
    );
    
  }

  getProductInfo(slug: string){
    return this.http.get('https://trainee-program-api.applaudostudios.com/api/v1/products/' + slug).
    pipe(
      map((response: any) => {
        return response;
        
      }),
      catchError((error) => {
        return throwError(error);
      }
      )
    );
  }

  createCart(product: InfoProductResponse){
    let data =  {
      "data": {
        "items": [
          {
          "product_variant_id": product.master.id,
          "quantity": 1
        }
      ]
    }
    };
    
    let header = {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + localStorage.getItem('token')),  
    }

    console.log(data);
    return this.http.post('https://trainee-program-api.applaudostudios.com/api/v1/cart', data, header).
    pipe(
      map((response: any) => {
        
        return response;
      }
      ),
      catchError((error) => {
        return throwError(error);
      }
      )
    );
  }

  modifyCart(product: QuantityUpdate | Add_UpdateProduct | RemoveProduct){
    let header = {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + localStorage.getItem('token')),  
    }

    console.log(product);
    return this.http.put('https://trainee-program-api.applaudostudios.com/api/v1/cart', product, header).
    pipe(
      map((response: any) => {
        console.log(response);
        return response;
      }
      ),
      catchError((error) => {
        return throwError(error);
      }
      )
    );    
  }

  getCartInfo(){
    let header = {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + localStorage.getItem('token')),  
    }

    return this.http.get('https://trainee-program-api.applaudostudios.com/api/v1/cart', header).
    pipe(
      map((response: any) => {
        //console.log(response);
        return response;
      }
      ),
      catchError((error) => {
        console.log("We need to create a new cart");
        return throwError(error);
      }
      )
    );
  }

  deleteCart(){
    let header = {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + localStorage.getItem('token')),  
    }

    return this.http.delete('https://trainee-program-api.applaudostudios.com/api/v1/cart', header).
    pipe(
      map((response: any) => {
        return response;
      }
      ),
      catchError((error) => {
        return throwError(error);
      }
      )
    );
    
  }
    
}
