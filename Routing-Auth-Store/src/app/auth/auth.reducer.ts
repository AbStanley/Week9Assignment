import { Action, createReducer, on } from '@ngrx/store';
import { UserResponse } from '../interface/user/user.interface';
import { AuthActions, AuthActionTypes } from './auth.actions';


export const authFeatureKey = 'auth';

export interface AuthState {
  isAuthenticated: boolean;
  user: UserResponse | undefined;
}

export const initialState: AuthState = {
  isAuthenticated: false,
  user: undefined
};

export function authReducer(state = initialState, action: AuthActions): AuthState {
 switch (action.type) {
  case AuthActionTypes.LoginAction: {
    return {
      isAuthenticated: true,
      user: action.payload.credentials
    };
  }
  case AuthActionTypes.LogOutAction: {
    return {
      isAuthenticated: false,
      user: undefined
    };
  }
   
   default:
     return state;
 }
}

/*
export const reducer = createReducer(
  initialState,

);
*/
