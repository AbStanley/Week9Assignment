import { AuthService } from 'src/app/services/auth.service';
import { select, Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { InfoProductResponse } from 'src/app/interface/produtResponse/infoProductResponse.interface';
import { DataResponse } from 'src/app/interface/produtResponse/dataResponse.interface';
import { MetaResponse } from 'src/app/interface/produtResponse/metaResponse.interface';
import { Product } from 'src/app/interface/store/product.interface';
import { AddProduct } from 'src/app/cart-state-store/cart.actions';
import { CartList } from 'src/app/interface/store/cartList.interface';
import { Observable } from 'rxjs';
import { isLoggedIn } from 'src/app/auth/auth.selectors';

@Component({
  selector: 'app-shop-products',
  templateUrl: './shop-products.component.html',
  styleUrls: ['./shop-products.component.scss']
})
export class ShopProductsComponent implements OnInit {
  
  constructor(private store: Store, public AuthService: AuthService) { }
  products!: InfoProductResponse[];
  shownProducts!: InfoProductResponse[];
  categoriesList: string[] = [];
  ngOnInit(): void {
    
    
  }

  
  returnProducts() {
    return this.shownProducts;
  }  

 
/*
  public buyProduct(product: CartList) {
    this.store.dispatch(new AddProduct(product));
  }
*/
  getListOfProducts(): void {
    this.AuthService.getListOfProducts().subscribe(
      (data: DataResponse<InfoProductResponse[]>) => {        
        this.products = data.data;
        this.shownProducts = this.products;
      //  console.log(this.products);
        this.categoriesList = this.shownProducts.map(product => {
            return product.category.name;
         });        
      },
      () => {
        this.products = [];
      }
    );  
  }

  likesManager($event: any, product: any, action: string) {
   // console.log(product);
    this.AuthService.likesManager(product, action).subscribe(
      (data: MetaResponse) => {
        //$event.target.parentElement.className = 'selected';
        this.AuthService.getProductInfo(product.slug).subscribe((data: any) => {
          product = data;
      //    console.log(product);
          if (action == 'up')
            $event.target.parentElement.innerText = data.data.likes_up_count;
          else
            $event.target.parentElement.innerText = data.data.likes_down_count;
        });
      }
    );
  }

  getCategory(value: string) {
    this.shownProducts = this.products.filter((product) => {
      return product.category.name.includes(value);
    });
  }

  /* IDEA*/

}
