import { UserResponse } from './../interface/user/user.interface';
import { User } from 'src/app/classes/users';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { AuthActions, AuthActionTypes } from '../auth/auth.actions';

/*
type AuthState = {
  isAuthenticated: boolean;
  user: UserResponse | undefined;
}
*/
export interface AppState {
//  auth: any;
}
/*
const initialAuthState: AuthState = {
  isAuthenticated: false,
  user: undefined
};

export function authReducer(state: AuthState = initialAuthState, action: any): AuthState {
  switch (action.type) {
    case AuthActionTypes.LoginAction: {
      return {
        isAuthenticated: true,
        user: action.payload.credentials
      };
    }
    default:
      return state;
  }
}
*/

export const reducers: ActionReducerMap<AppState> = {
//  auth: authReducer


};


export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
