export interface item {
    id: number;
    quantity: number;
    product_variant_id: number;
    order_id: number;
    total: number; // string?
    price: number; // string?
    name: string;
    description: string;
    promotion: number;

}