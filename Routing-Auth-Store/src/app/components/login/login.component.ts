import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule, FormBuilder, FormGroup } from '@angular/forms';
import { Register } from 'src/app/classes/register';
import { User } from 'src/app/classes/users';
import { Validators } from '@angular/forms';
import { AppState } from 'src/app/reducers';
import { Store } from '@ngrx/store';
import { Login } from 'src/app/auth/auth.actions';
import { UserResponse } from 'src/app/interface/user/user.interface';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  invalidLogin: boolean = false;

  signInForm: FormGroup = new FormGroup({});

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.signInForm = this.formBuilder.group({
      email: this.formBuilder.control('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]),
      password: this.formBuilder.control('', [Validators.required, Validators.minLength(8)], ),
    });
  }

  get email() {
    return this.signInForm.get('email');
  }

  get password() {
    return this.signInForm.get('password');
  }

  loginUser(): void {
    let credentials: User = {} as User;
    credentials.data = this.signInForm.value;

    
    this.authService.loginUser(credentials)
    .pipe(
      tap((credentials: UserResponse) => {
        if (credentials) {
          console.log(credentials);
          this.store.dispatch(new Login({credentials}));
          this.router.navigate(['/']);
        } else {
          this.invalidLogin = true;
        }
      })
    )
    .subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
        this.invalidLogin = true;
      }
    );
  }

  sendToRegister(): void {
    this.router.navigate(['register']);
  }
}
