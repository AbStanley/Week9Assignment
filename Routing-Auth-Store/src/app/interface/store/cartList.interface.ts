export interface CartList {
        id: number;
        user_id: number;
        number: number;
        status: string;
        total: string;
        total_items: string;
        completed_at?: any;
        created_at: Date;
        items: Item[];
}

export interface Item {
    id: number;
    quantity: number;
    product_variant_id: number;
    product_id: number;
    order_id: number;
    total: string;
    price: string;
    name: string;
    description: string;
    promotion: number;
}


