export interface RemoveProduct {
    "data": {
        "items": [
            {
                "id": number,
                "_destroy": boolean
            }
        ]
    }
}