import { userHomeInfo } from './../../classes/userHomeInfo';
import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { userInfo } from 'src/app/auth/auth.selectors';
import { select, Store } from '@ngrx/store';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  //@Input() userInfo!: userHomeInfo;
  @Output() onClickInsideProfileInfo = new EventEmitter<boolean>();

  userInfo$?: Observable<any>;


  constructor(private _elementRef: ElementRef,  private store: Store<any>) { }

  ngOnInit(): void {
    this.userInfo$ = this.store.pipe(select(userInfo));
  }

  @HostListener('document:click', ['$event.target'])
  clickProfileInfo(targetElement: any) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    console.log(clickedInside);
    if (!clickedInside) {
      this.onClickInsideProfileInfo.emit(false);
    }

  }
}

    