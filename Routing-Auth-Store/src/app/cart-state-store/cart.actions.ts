import { Product } from 'src/app/interface/store/product.interface';
import { Action, createAction, props } from '@ngrx/store';
import { InfoProductResponse } from '../interface/produtResponse/infoProductResponse.interface';
import { CartList, Item } from '../interface/store/cartList.interface';

export enum CartActionTypes {
  AddProduct = '[AddProduct] Action',
  RemoveProduct = '[RemoveProduct] Action',
  ClearCart = '[ClearCart] Action',
  UpdateProductList = '[UpdateCart] Action',

}

export class AddProduct implements Action {
    type = CartActionTypes.AddProduct;
    constructor(public payload: CartList) { }
}
export class RemoveProduct implements Action  {
    type = CartActionTypes.RemoveProduct;
    constructor(public payload: Item) { }
}

export class ClearCart implements Action  {
    type = CartActionTypes.ClearCart;
}

export class UpdateProductList implements Action {
    type = CartActionTypes.UpdateProductList;
    constructor(public payload: CartList) { }
}


export type CartActions = AddProduct | RemoveProduct | ClearCart | UpdateProductList;