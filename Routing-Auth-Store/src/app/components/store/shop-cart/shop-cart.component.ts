import { AuthService } from 'src/app/services/auth.service';
import { cartProducts, selectGroupedCartEntries } from './../../../cart-state-store/cart.selectors';
import {
  ClearCart,
  AddProduct,
  RemoveProduct,
} from './../../../cart-state-store/cart.actions';
import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ProductGroup } from 'src/app/cart-state-store/cart.selectors';
import { map } from 'rxjs/operators';
import { Add_UpdateProduct, QuantityUpdate } from 'src/app/interface/store/App_UpdateProduct.interface';
import { isLoggedIn } from 'src/app/auth/auth.selectors';
import { AppState } from 'src/app/reducers';
import { CartList } from 'src/app/interface/store/cartList.interface';

@Component({
  selector: 'app-shop-cart',
  templateUrl: './shop-cart.component.html',
  styleUrls: ['./shop-cart.component.scss'],
})
export class ShopCartComponent implements OnInit {
  cartEntries$?: Observable<ProductGroup[]>;
  isLoggedIn$?: Observable<boolean>;
  productsInCart?: any;
  showDelete: boolean = false;
  cartCreated: boolean = false;
  buyProduct() {
    alert("Buy");
  }
  cartProducts$?: Observable<any>;

  constructor(private store: Store<AppState>, private AuthService: AuthService) {
  }

  ngOnInit(): void {;
    this.isLoggedIn$ = this.store.pipe(select(isLoggedIn));
    this.loadCart();
    this.cartProducts$ = this.store.pipe(
      select(cartProducts)
    );
  }

  loadCart(): void {
    this.AuthService.getCartInfo().subscribe(
      (res) => {      
        this.productsInCart = res.data.items;
        this.showDelete = true;
        this.cartCreated = true;
      },
      () => {
        this.productsInCart = [];
        this.showDelete = false;
        this.cartCreated = false;
      }
    );
  }

  clearEntries() {
    if (this.productsInCart.length > 0) {
      
      this.store.dispatch(new ClearCart());

      this.AuthService.deleteCart().subscribe(
        (res) => {
          alert('Cart cleared');
          this.showDelete = false;
        },
        (err) => {
          alert('Error occurred');
        }
      );
    }

    
  }

  more(entry: any): void {
    this.productsInCart = this.productsInCart.map((item: any) => {
      if (item.id === entry.id) {
        return {
          ...item,
          quantity: item.quantity + 1
        };
      } else {
        return item;
      }
    });

    const update: QuantityUpdate = {
      data: {
        items: this.productsInCart
      }
    }
        
    this.AuthService.modifyCart(update).subscribe(
      (res) => {
      console.log(res);
      //this.store.dispatch(new AddProduct(entry));
      },
      (err) => {
        alert('Error occurred');
      }
    );
  }

  less(entry: any) {
    this.productsInCart = this.productsInCart.map((item: any) => {
      if (item.id === entry.id) {
        return {
          ...item,
          quantity: item.quantity > 1 ? item.quantity -1 : item.quantity
        };
      } else {
        return item;
      }
    });

    const update: QuantityUpdate = {
      data: {
        items: this.productsInCart
      }
    }    
    this.AuthService.modifyCart(update).subscribe(
      (res) => {
      this.store.dispatch(new AddProduct(entry));
      },
      (err) => {
        alert('Error occurred');
      }
    );
  }


  removeItem(item: any){
    this.productsInCart = this.productsInCart.filter((itemInCart: any) => {
      return itemInCart.id !== item.id;
    });

    let update: any = {
      "data": {
        "items": [
            {
                "id": item.id,
                "_destroy": true
            }
        ]
    }
    }    
    this.AuthService.modifyCart(update).subscribe(
      (res) => {
      this.store.dispatch(new RemoveProduct(item));
      },
      (err) => {
        alert('Error occurred');
      }
    );
  }
}
