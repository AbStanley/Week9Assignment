import { HttpHeaders } from "@angular/common/http";

export interface AppHttpOptions<T = any> {
    Headers?: HttpHeaders;
    Body?: T;
    RequestUrl: string;
    QueryParams?: object;
}