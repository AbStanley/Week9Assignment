import { Action } from '@ngrx/store';
import { UserResponse } from '../interface/user/user.interface';

export enum AuthActionTypes {
  LoginAction = '[Login] Action',
  LogOutAction = '[Logout] Action',
}

export class Login implements Action {
  readonly type = AuthActionTypes.LoginAction;

  constructor(public payload: {credentials: UserResponse}) {}
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LogOutAction;
}

export type AuthActions = Login | Logout;

