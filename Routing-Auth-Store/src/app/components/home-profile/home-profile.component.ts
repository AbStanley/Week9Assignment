import { Add_UpdateProduct } from './../../interface/store/App_UpdateProduct.interface';
import { initialCartEntries } from './../../cart-state-store/cart.reducer';
import { AddProduct, UpdateProductList } from './../../cart-state-store/cart.actions';
import { Category } from './../../interface/produtResponse/infoProductResponse.interface';
import { MetaResponse } from '../../interface/produtResponse/metaResponse.interface';
import { AuthService } from './../../services/auth.service';
import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { DataResponse } from 'src/app/interface/produtResponse/dataResponse.interface';
import { InfoProductResponse } from 'src/app/interface/produtResponse/infoProductResponse.interface';
import { DataService } from 'src/app/services/data.service';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/reducers';
import { ProductGroup } from 'src/app/cart-state-store/cart.selectors';
import { CartList } from 'src/app/interface/store/cartList.interface';

@Component({
  selector: 'app-home-profile',
  templateUrl: './home-profile.component.html',
  styleUrls: ['./home-profile.component.scss'],
})
export class HomeProfileComponent {
  products!: InfoProductResponse[];
  shownProducts!: InfoProductResponse[];



  subscription!: Subscription;

  categories = new FormControl();
  categoriesList: string[] = [];

  cartCreated: boolean = false;
 info?: any;


  constructor(
    public AuthService: AuthService,
    public DataManager: DataService,
    private store: Store<AppState>
  ) {
   
    this.subscription = this.DataManager.getData().subscribe(
      (searchData: string) => {
        this.shownProducts = this.products.filter((product) => {
          return product.name.toLowerCase().includes(searchData.toLowerCase());
        });
      }
    );
     
  }

  getCategory(value: string) {
    this.shownProducts = this.products.filter((product) => {
      return product.category.name.includes(value);
    });
  }

  ngOnInit() {
    this.verifyCartCreated();     
    this.getListOfProducts();
  }

  getListOfProducts(): void {
    this.AuthService.getListOfProducts().subscribe(
      (data: DataResponse<InfoProductResponse[]>) => {
        this.products = data.data;
        this.shownProducts = this.products;
       
        this.categoriesList = this.shownProducts.map((product) => {
          return product.category.name;
        });
      },
      () => {
        this.products = [];
      }
    );
  }

  likesManager($event: any, product: any, action: string) {
    this.AuthService.likesManager(product, action).subscribe(
      (data: MetaResponse) => {
        this.AuthService.getProductInfo(product.slug).subscribe((data: any) => {
          product.likes_up_count = data.data.likes_up_count;
          product.likes_down_count = data.data.likes_down_count;
        });
      }
    );
  }

  addProduct(product: InfoProductResponse) {
    
    if (!this.cartCreated) {
      this.AuthService.createCart(product).subscribe(
        (cartInfo: CartList) => {
          console.log(cartInfo);
          this.store.dispatch(new UpdateProductList(cartInfo));
        },
        () => {
          alert('Error, please try again');
        }
      );
    } else {
      this.addProductToExistingCart(product);
    }
  }

  addProductToExistingCart(product: InfoProductResponse) {
    let newProduct: Add_UpdateProduct = {
      "data": {
        "items": [
          {
            "product_variant_id": Number(product.master.id),
            "quantity": 1,
          },
        ],
      },
    };

    
    this.AuthService.modifyCart(newProduct).subscribe(
      (cartInfo: CartList) => {
        console.log(cartInfo);
        this.store.dispatch(new UpdateProductList(cartInfo));
      },
      () => {
        alert('Item already added');
      }
    );
  }

  verifyCartCreated() {
    this.AuthService.getCartInfo().subscribe(
      (cartInfo: any) => {
        if (cartInfo.data.items.length > 0) {
          this.cartCreated = true;
          
        }
      },
      () => {
        console.log('empty array');
        this.cartCreated = false;
      }
    );
  }
  getCartInfo() {
    this.AuthService.getCartInfo().subscribe(
      (cartInfo: any) => {
        console.log(cartInfo.data.items);
        return cartInfo;
      }
      
    );
  }
}
