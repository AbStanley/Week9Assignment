import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { passwordValidator } from 'src/app/validators/password.validator';
import { Register } from 'src/app/classes/register';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({});
  isError: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: this.formBuilder.control('', [Validators.required]),
      lastName: this.formBuilder.control('', [Validators.required]),
      userName: this.formBuilder.control('', [Validators.required]),
      email: this.formBuilder.control('', [
        Validators.required,
        Validators.email,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'),
      ]),
      passwords: this.formBuilder.group(
        {
          password: ['', [Validators.required, Validators.minLength(8)]],
          confirmPassword: ['', [Validators.required]],
        },
        {
          validators: passwordValidator.notSamePassword,
        }
      ),
    });
  }

  get firstName() {
    return this.registerForm.get('firstName');
  }

  get lastName() {
    return this.registerForm.get('lastName');
  }

  get userName() {
    return this.registerForm.get('userName');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get passwords() {
    return this.registerForm.get('passwords');
  }

  get password() {
    return this.registerForm.get('passwords.password');
  }

  get confirmPassword() {
    return this.registerForm.get('passwords.confirmPassword');
  }

  registerUser(): void {
    let registerInfo: Register = new Register();
    registerInfo.name =
      this.registerForm.value.firstName +
      ' ' +
      this.registerForm.value.lastName;
    registerInfo.email = this.registerForm.value.email;
    registerInfo.password = this.registerForm.value.passwords.password;
    registerInfo.passwordConfirmation =
      this.registerForm.value.passwords.confirmPassword;

    this.authService.registerUser(registerInfo).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(['/login']);
        this.isError = false;
      },
      (error) => {
        this.isError = true;
      }
    );
  }
}
