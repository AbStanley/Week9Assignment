
import { ActionReducer, createReducer, INIT, on, UPDATE } from '@ngrx/store';
import { InfoProductResponse } from '../interface/produtResponse/infoProductResponse.interface';
import { CartActionTypes } from './cart.actions';

export const initialCartEntries: any[] = [];
export const cartItems: any[] = [];

export const cartFeatureKey = 'cart';

export function cartReducer(state = initialCartEntries, action: any) {
    
    switch (action.type) {
        case CartActionTypes.AddProduct:{      
            let product = state.find(item => item.id === action.payload.id);
            if(!product){
                return [...state, action.payload];
            }
            else{                
                return [...state];
            }
            
        }
        case CartActionTypes.RemoveProduct:
            return state.filter(entry => entry.id !== action.payload.id);
        case CartActionTypes.ClearCart:
            return [];
        case CartActionTypes.UpdateProductList:
            return [action.payload]
        default:
            return state;
    }
}



export const metaReducerLocalStorage = (reducer: ActionReducer<any>): ActionReducer<any> => {
    return (state, action) => {
        if (action.type === INIT || action.type == UPDATE){
            const localStorageState = localStorage.getItem('state');
            if (localStorageState) {
                try {
                    return JSON.parse(localStorageState);
                } catch {
                    localStorage.removeItem('state');
                }            
            }
        }
        const nextState = reducer(state, action);
        localStorage.setItem('state', JSON.stringify(nextState));
        return nextState;
    };
};