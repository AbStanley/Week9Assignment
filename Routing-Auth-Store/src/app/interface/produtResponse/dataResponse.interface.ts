import { MetaResponse } from "./metaResponse.interface";

export interface DataResponse<T> {
    data: T;
    meta: MetaResponse
}
