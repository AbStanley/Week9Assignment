export interface userHomeInfo {
    id: number;
    email: string;
    name: string;
}