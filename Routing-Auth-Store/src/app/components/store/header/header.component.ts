import { InfoProductResponse } from 'src/app/interface/produtResponse/infoProductResponse.interface';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  listOfProductsCart,
  selectCountProducts,
  selectTotalPrice,
} from 'src/app/cart-state-store/cart.selectors';

import { AppState } from 'src/app/reducers';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  countProducts$?: Observable<number>;
  totalPrice$?: Observable<number>;
  listOfProductsCart$?: Observable<InfoProductResponse[]>;

  constructor(private store: Store<AppState>) { 
    
    
  }

  ngOnInit(): void {
    this.countProducts$ = this.store.select(selectCountProducts);
    this.totalPrice$ = this.store.select(selectTotalPrice);
    this.listOfProductsCart$ = this.store.select(listOfProductsCart);
  }

}
